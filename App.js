import React from 'react';
import { View } from 'react-native';
import Main from './app/route';
export default class App extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Main />
      </View>
    );
  }
}